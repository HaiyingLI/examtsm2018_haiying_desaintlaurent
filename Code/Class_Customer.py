###############################################################
# CLASS MODULE:    CUSTOMERS                                  #
###############################################################

"""
Each customer has the following characteristics:

 - customerID      (string)
 - budget          (integer)
 - buying_history  (array)

 These attributes can be grouped in pd.categorical variable :
 - returning       (boolean)
 - hipsters        (boolean)
 - tripadvisor     (boolean)


And the following properties:

- hipster          only if   returning
- tripadvisor      only if   one-time
- if one-time      then      budget=100
- if returning     then      budget=250
- if hipsters      then      budget=500

It must also be able to record a coffee-bar visit:
- 1 determine the consumption choices according to a set of probabilities
- 2 add the consumed products to the buying history along with the cumulated price and the date
- 3 determine the tip if necessary and actualize the budget
"""

##########################
# IMPORTS                #
##########################
import random


##############################
# ATTRIBUTES & INSTANTIATION #
##############################
class Customer(object):
    # Class Attribute to store the budget and profile settings
    settings = [['onetime', 'tripadvisor', 'returning', 'hipster'], [100, 100, 250, 500], [0, 10, 0, 0], [
        'frappucino', 'soda', 'coffee', 'tea', 'water', 'milkshake', 'nothing', 'sandwich', 'pie', 'muffin', 'cookie']]

    # Instantiation (instance attributes)
    def __init__(self, identifier):
        # Initialise instance attributes
        self.customerID = identifier
        self.buying_history = [[], [], [], []]
        self.profile = "onetime"
        self.budget = 100

    def set_profile(self, profile):
        for (i, j) in enumerate(self.settings[0]):
            if profile == j:
                self.profile = profile
                self.budget = self.settings[1][i]

##########################
# CONSUMPTION            #
##########################
    def coffee_bar(self, probas, date, prices):
        # Add date
        self.buying_history[0].append(date)
        price = 0
        # Choice of drinks
        rand = random.uniform(0, 1)  # Create a random float between 0 and 1
        temp = 0
        for (j, i) in enumerate(probas[2:8]):
            temp = temp + i
            if rand < temp:
                # Add drink
                self.buying_history[1].append(self.settings[3][j])
                # Add to bill using the price table
                price = price + prices[1][j]
                break
        # Choice of food
        temp = 0
        rand = random.uniform(0, 1)
        for (j, i) in enumerate(probas[8:13]):
            temp = temp + i
            if rand < temp:
                # Add food
                self.buying_history[2].append(self.settings[3][j+6])
                # Add to bill using the price table
                price = price + prices[1][j+6]
                break
        # Calculation of Tip
        for (i, j) in enumerate(self.settings[0]):
            if self.profile == j:
                price = price + float("{0:.2f}".format(random.uniform(0, self.settings[2][i])))
        # Add bill
        self.buying_history[3].append(price)
        # Actualize budget
        self.budget = self.budget - price



